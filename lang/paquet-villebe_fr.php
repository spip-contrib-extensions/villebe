<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'villebe_description' => 'Gestion des villes de belgiques via une saisies. Basé sur la liste [Bpost->http://www.bpost.be/site/fr/business/customer_service/search/postal_codes.html]',
	'villebe_nom' => 'Ville de belgique',
	'villebe_slogan' => 'Toutes les villes de Belgique avec leur code postal et province !',
);